from django.db import models


# Create your models here.
class Books(models.Model):
    title = models.CharField(max_length=256, verbose_name='书名')
    author = models.CharField(max_length=256, verbose_name='作者')
    press = models.CharField(max_length=256, verbose_name='出版社')
    original = models.CharField(max_length=256, verbose_name='原作名')
    translator = models.CharField(max_length=256, verbose_name='译者')
    imprint = models.CharField(max_length=12, verbose_name='出版年')
    pages = models.CharField(max_length=12, verbose_name='页数')
    price = models.CharField(max_length=12, verbose_name='定价')
    binding = models.CharField(max_length=12, verbose_name='装帧')
    series = models.CharField(max_length=256, verbose_name='丛书')
    isbn = models.CharField(max_length=16, verbose_name='ISBN')
    score = models.CharField(max_length=12, verbose_name='评分')
    number = models.CharField(max_length=12, verbose_name='评论人数')

    class Meta:
        db_table = "books"  # 更改表名
