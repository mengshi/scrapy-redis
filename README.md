# scrapy-redis

#### 介绍
豆瓣读书分布式爬取

#### 软件架构

book_details ：书籍详情（内置mysql保存管道，settings.py中设置）
book_url：书籍URL
web_books：web书籍查看
tags_url：书籍便签URL
save_book_mysql：将保存在redis中的数据提取到mysql（外置）

#### 安装教程

1. 库要求：scrapy-redis;scrapy-mysql-pipeline
2. xxxx
3. xxxx

#### 使用说明

1. 爬取前请根据需要修改代理API端口
2. 启动详情页爬取(可启动多个)：scrapy crawl books
3. 启动书籍URL爬取(可启动多个)：scrapy crawl books_url
4. 启动书籍便签爬取：tags_url.py
5. 备注：save_book_mysql.py为外置的将redis数据保存到mysql
#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)