# -*- coding: utf-8 -*-
import re
from book_details.items import BookDetailsItem
from scrapy_redis.spiders import RedisSpider
import datetime


class BooksSpider(RedisSpider):
    name = 'books'
    redis_key = 'book:book_url'
    date = datetime.datetime.now()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def parse(self, response):
        books_items = BookDetailsItem()
        vo = response.css('#wrapper')
        books_items['id'] = vo.re_first('name="pbtn-(\d+)-wish"')
        books_items['title'] = vo.css('h1 span::text').get(default='')
        books_items['author'] = vo.css('#info a::text').get(default='').replace('\n', '').replace(' ', '')
        info = response.css('#info').get(default='')
        books_items['press'] = ''.join(re.findall('<span.*?出版社.*?</span>\s*(.*?)\s*<br>', info))
        books_items['original'] = ''.join(re.findall('<span.*?原作名.*?</span>\s*(.*?)\s*<br>', info))
        books_items['translator'] = vo.css('#info span a::text').get(default='').replace('\n', '').replace(' ', '')
        books_items['imprint'] = ''.join(re.findall('<span.*?出版年.*?</span>\s*(.*?)\s*<br>', info))
        books_items['pages'] = ''.join(re.findall('<span.*?页数.*?</span>\s*(.*?)\s*<br>', info))
        books_items['price'] = ''.join(re.findall('<span.*?定价.*?</span>\s*(.*?)\s*<br>', info))
        books_items['binding'] = ''.join(re.findall('<span.*?装帧.*?</span>\s*(.*?)\s*<br>', info))
        books_items['series'] = ''.join(re.findall('<span.*?丛书.*?</span>.*?<a.*?>(.*?)</a>', info))
        books_items['isbn'] = ''.join(re.findall('<span.*?ISBN.*?</span>\s*(.*?)\s*<br>', info))
        rating = response.css('#interest_sectl')
        books_items['score'] = rating.css('strong::text').get(default='').replace(' ', '')
        books_items['number'] = rating.css('.rating_sum span a span::text').get(default='').replace(' ', '')
        print('time：', '{0}:{1}.txt'.format(self.date.minute, self.date.second), 'id：', books_items['id'],
              'title：', books_items['title'])
        yield books_items
