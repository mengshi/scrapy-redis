"""
将Redis中的Item信息遍历写入到数据库中
"""
# -*- coding: utf-8 -*-
# @Author: yqbao
# @GiteeURL: https://gitee.com/yqbao
# @Date: 2019/9/12 17:10
# @Version: v.0.0

import json
import redis
import pymysql


def main():
    redis_cli = redis.StrictRedis(host='127.0.0.1', port=6379, db=0)  # 指定Redis数据库信息
    db = pymysql.connect(host="localhost", user="root", password="password", db="douban",
                         charset="utf8")  # 指定MySQL数据库信息
    cursor = db.cursor()  # 使用cursor()方法创建一个游标对象curso
    while True:
        source, data = redis_cli.blpop(["books:items"])  # FIFO模式为 blpop，LIFO模式为 brpop，获取键值
        try:
            item = json.loads(data)
            dd = dict(item)  # 组装sql语句
            keys = ','.join(dd.keys())
            values = ','.join(['%s'] * len(dd))
            sql = "insert into books(%s) values(%s)" % (keys, values)
            cursor.execute(sql, tuple(dd.values()))  # 指定参数，并执行sql添加
            db.commit()  # 事务提交
            print("写入信息成功：", dd['id'])
        except Exception as e:
            db.rollback()  # 事务回滚
            print("SQL执行错误，原因：", e.args)


if __name__ == '__main__':
    main()  # 主程序入口
